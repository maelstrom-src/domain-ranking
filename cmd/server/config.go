package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Host      string   `json:"host"`
	Port      int      `json:"port"`
	Providers []string `json:"providers"`
}

func NewConfig(host string, port int, providers []string) *Config {
	return &Config{
		Host:      host,
		Port:      port,
		Providers: providers,
	}
}

func LoadConfigFromFile(filename string) (*Config, error) {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var config Config

	err = json.Unmarshal(bytes, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func (c *Config) ListenAddress() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}
