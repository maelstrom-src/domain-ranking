package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
	"gitlab.com/maelstrom-src/domain-ranking/internal/serde"
	"gitlab.com/maelstrom-src/domain-ranking/internal/server"
	"gitlab.com/maelstrom-src/domain-ranking/internal/version"
	"google.golang.org/grpc"
)

var flagConfig = flag.String("config", "", "configuration file to load")

var stop chan struct{}

// TODO: Not sure but maybe it does not release memory after reload???
func reloadLoop() {
	reload := make(chan bool, 1)
	reload <- true

	for <-reload {
		reload <- false

		ctx, cancel := context.WithCancel(context.Background())

		signals := make(chan os.Signal, 1)
		signal.Notify(signals, os.Interrupt, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT)
		go func() {
			select {
			case sig := <-signals:
				log.Printf("Received signal: %v", sig)
				if sig == syscall.SIGHUP {
					log.Printf("Reloading DR-Server...")
					<-reload
					reload <- true
				}
				cancel()
			case <-stop:
				log.Printf("Received stop")
				cancel()
			}
		}()

		err := runServer(ctx)
		if err != nil && err != context.Canceled {
			log.Fatalf("Error running DR-Server: %v", err)
		}
	}
}

func runServer(ctx context.Context) error {
	log.Printf("Starting DR-Server %s", version.Get())

	config, err := LoadConfigFromFile(*flagConfig)
	if err != nil {
		return err
	}

	rankings := loadRankingsFiles(config.Providers)
	if len(rankings) == 0 {
		return fmt.Errorf("No rankings")
	}

	lis, err := net.Listen("tcp", config.ListenAddress())
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()

	server.Register(grpcServer, rankings)

	go func() {
		for {
			select {
			case <-ctx.Done():
				log.Printf("Stopping DR-Server")
				grpcServer.GracefulStop()
				return // returning not to leak the goroutine
			}
		}
	}()

	if err := grpcServer.Serve(lis); err != nil {
		return err
	}

	return nil
}

func loadRankingsFiles(filenames []string) []*ranking.DomainRankings {
	var rankings []*ranking.DomainRankings

	for _, filename := range filenames {
		loadedRankings, err := loadRankingsFile(filename)
		if err != nil {
			log.Printf("Failed to load %s (%s)", filename, err)
			continue
		}
		rankings = append(rankings, loadedRankings)
	}

	return rankings
}

func loadRankingsFile(filename string) (*ranking.DomainRankings, error) {
	log.Printf("Loading rankings file %s...", filename)

	domainRankings, err := deserializeFromJSONFile(filename)
	if err != nil {
		return nil, err
	}

	log.Printf("Loaded rankings file %s:", filename)
	log.Printf("Provider: %s", domainRankings.Provider)
	log.Printf("Reference: %s", domainRankings.Reference)
	log.Printf("PublishedAt: %s", domainRankings.PublishedAt)
	log.Printf("CreatedAt: %s", domainRankings.CreatedAt)
	log.Printf("RankingType: %s", domainRankings.RankingType)

	return domainRankings, nil
}

func deserializeFromJSONFile(filename string) (*ranking.DomainRankings, error) {
	jsonSerDe := serde.NewJSONSerDe()
	return jsonSerDe.DeserializeFromFile(filename)
}

func run() {
	stop = make(chan struct{})

	reloadLoop()
}

func main() {
	flag.Parse()

	run()
}
