package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/urfave/cli/v2"
	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/client"
)

const (
	domainCommandName      = "domain"
	domainCommandAlias     = "d"
	domainCommandUsage     = "Get ranks by domain"
	domainCommandArgsUsage = "[domain]"
)

var DomainCommand = &cli.Command{
	Name:      domainCommandName,
	Aliases:   []string{domainCommandAlias},
	Usage:     domainCommandUsage,
	ArgsUsage: domainCommandArgsUsage,
	Flags: []cli.Flag{
		providerFlag,
	},
	Action: domainAction,
}

func domainAction(c *cli.Context) error {
	args := c.Args()
	if !args.Present() {
		return icli.ErrMissingArgs("No domain provided")
	}

	domains := make([]string, args.Len())

	for i, domain := range args.Slice() {
		d := strings.TrimSpace(domain)
		if d == "" {
			return icli.ErrInvalidArgs("Empty domain")
		}

		domains[i] = d
	}

	log.Print(domains)

	target := c.String(targetFlagName)

	drClient := client.NewClient(target)

	if err := drClient.Connect(); err != nil {
		return icli.ErrIOFailure(err.Error())
	}

	defer drClient.Close()

	provider := c.String(providerFlagName)
	if provider != "" {
		for _, domain := range domains {
			ranking, err := drClient.GetProviderRankingByDomain(provider, domain)
			if err != nil {
				if err == client.ErrNoRankingFound {
					printDomainNoRanking(domain)
					continue
				} else {
					return icli.ErrIOFailure(err.Error())
				}
			}

			printRanking(ranking)
		}
	} else {
		for _, domain := range domains {
			rankings, err := drClient.GetRankingByDomain(domain)
			if err != nil {
				if err == client.ErrNoRankingFound {
					printDomainNoRanking(domain)
					continue
				} else {
					return icli.ErrIOFailure(err.Error())
				}
			}

			for _, ranking := range rankings {
				printRanking(ranking)
			}
		}
	}

	return nil
}

func printDomainNoRanking(domain string) {
	fmt.Printf("%s\tno ranking\n", domain)
}
