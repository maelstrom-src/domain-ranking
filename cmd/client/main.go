package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := cli.NewApp()
	app.Usage = "Domain ranking client"
	app.Version = "0.1.0"
	app.Flags = []cli.Flag{
		targetFlag,
	}
	app.Commands = []*cli.Command{
		ProviderCommand,
		RankCommand,
		DomainCommand,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal("Internal error: " + err.Error())
	}
}
