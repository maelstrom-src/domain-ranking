package main

import "github.com/urfave/cli/v2"

// Global flags.
const (
	targetFlagName = "target"
)

// Glabal flag default values.
const (
	targetDefaultValue = "localhost:10000"
)

var (
	targetFlag = &cli.StringFlag{
		Name:  targetFlagName,
		Usage: "Target Domain Ranking server",
		Value: targetDefaultValue,
	}
)

// Command specific flags.
const (
	providerFlagName = "provider"
	offsetFlagName   = "offset"
	limitFlagName    = "limit"
)

var (
	providerFlag = &cli.StringFlag{
		Name:  providerFlagName,
		Usage: "Provider",
	}
	offsetFlag = &cli.UintFlag{
		Name:  offsetFlagName,
		Usage: "Ranking offset",
	}
	limitFlag = &cli.UintFlag{
		Name:  limitFlagName,
		Usage: "Ranking limit",
	}
)
