package main

import (
	"fmt"

	"github.com/urfave/cli/v2"
	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/client"
)

const (
	providerCommandName      = "provider"
	providerCommandAlias     = "p"
	providerCommandUsage     = "Get providers"
	providerCommandArgsUsage = "[provider]"
)

var ProviderCommand = &cli.Command{
	Name:      providerCommandName,
	Aliases:   []string{providerCommandAlias},
	Usage:     providerCommandUsage,
	ArgsUsage: providerCommandArgsUsage,
	Flags: []cli.Flag{
		offsetFlag,
		limitFlag,
	},
	Action: providerAction,
}

func providerAction(c *cli.Context) error {
	args := c.Args()

	target := c.String(targetFlagName)

	client := client.NewClient(target)

	if err := client.Connect(); err != nil {
		return icli.ErrIOFailure(err.Error())
	}

	defer client.Close()

	if args.Present() {
		providerName := args.First()

		offset := c.Uint(offsetFlagName)
		limit := c.Uint(limitFlagName)

		if limit != 0 {
			rankings, err := client.GetProviderRankings(providerName, uint32(offset), uint32(limit))
			if err != nil {
				// TODO: proper error.
				return icli.ErrIOFailure(err.Error())
			}

			for _, ranking := range rankings {
				printRanking(ranking)
			}
		} else {
			provider, err := client.GetProvider(providerName)
			if err != nil {
				// TODO: proper error.
				return icli.ErrIOFailure(err.Error())
			}

			printProvider(provider)
		}
	} else {
		providers, err := client.GetProviders()
		if err != nil {
			// TODO: proper error.
			return icli.ErrIOFailure(err.Error())
		}

		for _, provider := range providers {
			fmt.Printf("%s\n", provider.Provider)
		}
	}

	return nil
}

// TODO: utils???
func printProvider(provider *client.Provider) {
	fmt.Printf("Provider\t%s\n", provider.Provider)
	fmt.Printf("Reference\t%s\n", provider.Referene)
	fmt.Printf("Ranking Type\t%s\n", provider.RankingType)
	fmt.Printf("Rankings\t%d\n", provider.RankingCount)
	fmt.Printf("Published At\t%s\n", provider.PublishedAt)
	fmt.Printf("Created At\t%s\n", provider.CreatedAt)
}
