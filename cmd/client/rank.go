package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/urfave/cli/v2"
	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/client"
)

const (
	rankCommandName      = "rank"
	rankCommandAlias     = "r"
	rankCommandUsage     = "Get domains by rank"
	rankCommandArgsUsage = "[rank]"
)

var RankCommand = &cli.Command{
	Name:      rankCommandName,
	Aliases:   []string{rankCommandAlias},
	Usage:     rankCommandUsage,
	ArgsUsage: rankCommandArgsUsage,
	Flags: []cli.Flag{
		providerFlag,
	},
	Action: rankAction,
}

func rankAction(c *cli.Context) error {
	args := c.Args()
	if !args.Present() {
		return icli.ErrMissingArgs("No rank provided")
	}

	ranks := make([]uint32, args.Len())

	for i, rank := range args.Slice() {
		r, err := strconv.ParseUint(rank, 10, 32)
		if err != nil {
			return icli.ErrInvalidArgs(err.Error())
		}

		ranks[i] = uint32(r)
	}

	log.Print(ranks)

	target := c.String(targetFlagName)

	drClient := client.NewClient(target)

	if err := drClient.Connect(); err != nil {
		return icli.ErrIOFailure(err.Error())
	}

	defer drClient.Close()

	provider := c.String(providerFlagName)
	if provider != "" {
		for _, rank := range ranks {
			ranking, err := drClient.GetProviderRankingByRank(provider, rank)
			if err != nil {
				if err == client.ErrNoRankingFound {
					printRankNoRanking(rank)
					continue
				} else {
					return icli.ErrIOFailure(err.Error())
				}
			}

			printRanking(ranking)
		}
	} else {
		for _, rank := range ranks {
			rankings, err := drClient.GetRankingByRank(rank)
			if err != nil {
				if err == client.ErrNoRankingFound {
					printRankNoRanking(rank)
					continue
				} else {
					return icli.ErrIOFailure(err.Error())
				}
			}

			for _, ranking := range rankings {
				printRanking(ranking)
			}
		}
	}

	return nil
}

func printRanking(r *client.Ranking) {
	fmt.Printf("%s\t%d\t%s\t%s\n", r.Domain, r.Rank, r.Provider, r.PublishedAt)
}

func printRankNoRanking(rank uint32) {
	fmt.Printf("%d\tno ranking\n", rank)
}
