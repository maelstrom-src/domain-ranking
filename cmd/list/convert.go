package main

import (
	"io"
	"os"

	"github.com/urfave/cli/v2"
	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

const (
	convertCommandName  = "convert"
	convertCommandAlias = "c"
	convertCommandUsage = "Convert domain ranking list format"
)

var ConvertCommand = &cli.Command{
	Name:    convertCommandName,
	Aliases: []string{convertCommandAlias},
	Usage:   convertCommandUsage,
	Flags: []cli.Flag{
		inputFlag,
		inputFormatFlag,
		outputFlag,
		outputFormatFlag,
		providerFlag,
		referenceFlag,
		rankingTypeFlag,
		publishedFlag,
	},
	Action: convertAction,
}

func convertAction(c *cli.Context) error {
	inputFormat := c.String(inputFormatFlagName)
	if !IsIOFormatValid(inputFormat) {
		return icli.ErrInvalidInputFormat(inputFormat)
	}

	outputFormat := c.String(outputFormatFlagName)
	if !IsIOFormatValid(outputFormat) {
		return icli.ErrInvalidOutputFormat(outputFormat)
	}

	if inputFormat == outputFormat {
		return icli.ErrInvalidIOFormat("Same format for input and ouput: " + inputFormat)
	}

	var err error
	var reader io.ReadCloser

	input := c.String(inputFlagName)
	if IsStdIO(input) {
		reader = os.Stdin
	} else {
		reader, err = OpenFile(input)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	var writer io.WriteCloser

	output := c.String(outputFlagName)
	if IsStdIO(output) {
		writer = os.Stdout
	} else {
		writer, err = CreateFile(output)
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	var domainRankings *ranking.DomainRankings

	domainRankings, err = Deserialize(inputFormat, reader)
	if err != nil {
		return err
	}

	if inputFormat == IOFormatCSV {
		provider := c.String(providerFlagName)
		reference := c.String(referenceFlagName)

		rankingType := NormalizeRankingType(c.String(rankingTypeFlagName))
		if !IsRankingTypeValid(rankingType) {
			return icli.ErrInvalidArgs("Invalid domain ranking type: " + rankingType.String())
		}

		published := c.Timestamp(publishedFlagName)
		if published == nil {
			now := NowUTC()
			published = &now
		}

		domainRankings.Provider = provider
		domainRankings.Reference = reference
		domainRankings.RankingType = rankingType
		domainRankings.PublishedAt = *published
	}

	PrintDomainRankings(domainRankings)

	return Serialize(outputFormat, writer, domainRankings)
}
