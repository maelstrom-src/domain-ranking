package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"

	_ "gitlab.com/maelstrom-src/domain-ranking/internal/providers/all"
)

func main() {
	app := cli.NewApp()
	app.Usage = "Manage domain ranking lists"
	app.Version = "0.1.0"
	app.Commands = []*cli.Command{
		ListCommand,
		GetCommand,
		ConvertCommand,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal("Internal error: " + err.Error())
	}
}
