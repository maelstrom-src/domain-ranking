package main

import (
	"strings"

	"github.com/urfave/cli/v2"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

const (
	inputFlagName        = "in"
	inputFormatFlagName  = "inform"
	outputFlagName       = "out"
	outputFormatFlagName = "outform"
	providerFlagName     = "provider"
	referenceFlagName    = "reference"
	rankingTypeFlagName  = "ranking-type"
	publishedFlagName    = "published"
)

const (
	IOFormatJSON = "json"
	IOFormatCSV  = "csv"
)

const (
	StdIO = "-"
)

const (
	inputDefaultValue        = StdIO
	inputFormatDefaultValue  = IOFormatJSON
	outputDefaultValue       = StdIO
	outputFormatDefaultValue = IOFormatJSON
	providerDefaultValue     = "custom_provider"
	referenceDefaultValue    = "custom_reference"
	rankingTypeDefaultValue  = "MIX"
)

const timestampFlagLayout = "2006-01-02T15:04:05Z"

var (
	inputFlag = &cli.StringFlag{
		Name:  inputFlagName,
		Usage: "Input file or read from STDIN (-)",
		Value: inputDefaultValue,
	}
	inputFormatFlag = &cli.StringFlag{
		Name:  inputFormatFlagName,
		Usage: "Input format, JSON or CSV",
		Value: inputFormatDefaultValue,
	}
	outputFlag = &cli.StringFlag{
		Name:  outputFlagName,
		Usage: "Output file or write to STDOUT (-)",
		Value: outputDefaultValue,
	}
	outputFormatFlag = &cli.StringFlag{
		Name:  outputFormatFlagName,
		Usage: "Output format, JSON or CSV",
		Value: outputFormatDefaultValue,
	}
	providerFlag = &cli.StringFlag{
		Name:  providerFlagName,
		Usage: "Provider, applicable when converting from CSV",
		Value: providerDefaultValue,
	}
	referenceFlag = &cli.StringFlag{
		Name:  referenceFlagName,
		Usage: "Reference, applicable when converting from CSV",
		Value: referenceDefaultValue,
	}
	rankingTypeFlag = &cli.StringFlag{
		Name:  rankingTypeFlagName,
		Usage: "Ranking type, applicable when converting from CSV",
		Value: rankingTypeDefaultValue,
	}
	publishedFlag = &cli.TimestampFlag{
		Name:   publishedFlagName,
		Usage:  "Published time, applicable when converting from CSV",
		Layout: timestampFlagLayout,
	}
)

func NormalizeIOFormat(ioFromat string) string {
	return strings.ToLower(ioFromat)
}

func IsIOFormatValid(ioFormat string) bool {
	n := NormalizeIOFormat(ioFormat)
	return n == IOFormatCSV || n == IOFormatJSON
}

func IsStdIO(io string) bool {
	return io == StdIO
}

func NormalizeRankingType(rankingType string) ranking.DomainRankingType {
	return ranking.DomainRankingType(strings.ToUpper(rankingType))
}

func IsRankingTypeValid(rankingType ranking.DomainRankingType) bool {
	return rankingType == ranking.TopLevelDomains || rankingType == ranking.PayLevelDomains || rankingType == ranking.MixLevelDomains
}
