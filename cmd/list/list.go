package main

import (
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/maelstrom-src/domain-ranking/internal/providers"
)

const (
	listCommandName  = "list"
	listCommandAlias = "l"
	listCommandUsage = "List available domain ranking providers"
)

var ListCommand = &cli.Command{
	Name:    listCommandName,
	Aliases: []string{listCommandAlias},
	Usage:   listCommandUsage,
	Action:  listAction,
}

func listAction(c *cli.Context) error {
	availableProviders := providers.Available()
	for _, provider := range availableProviders {
		fmt.Println(provider)
	}
	return nil
}
