package main

import (
	"io"
	"log"
	"os"
	"time"

	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/csv"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
	"gitlab.com/maelstrom-src/domain-ranking/internal/serde"
)

func CreateFile(name string) (*os.File, error) {
	file, err := os.Create(name)
	if err != nil {
		return nil, icli.ErrIOFailure(err.Error())
	}
	return file, nil
}

func OpenFile(name string) (*os.File, error) {
	file, err := os.Open(name)
	if err != nil {
		return nil, icli.ErrIOFailure(err.Error())
	}
	return file, nil
}

func Serialize(format string, writer io.Writer, domainRankings *ranking.DomainRankings) error {
	if format == IOFormatJSON {
		return SerializeJSON(writer, domainRankings)
	} else {
		return SerializeCSV(writer, domainRankings)
	}
}

func SerializeJSON(writer io.Writer, domainRankings *ranking.DomainRankings) error {
	jsonSerDe := serde.NewJSONSerDe()
	err := jsonSerDe.SerializeToWriter(writer, domainRankings)
	if err != nil {
		return icli.ErrIOFailure(err.Error())
	}
	return nil
}

func SerializeCSV(writer io.Writer, domainRankings *ranking.DomainRankings) error {
	err := csv.WriteRankingsCSV(writer, domainRankings.Rankings)
	if err != nil {
		return icli.ErrIOFailure(err.Error())
	}
	return nil
}

func Deserialize(format string, reader io.Reader) (*ranking.DomainRankings, error) {
	if format == IOFormatJSON {
		return DeserializeJSON(reader)
	} else {
		return DeserializeCSV(reader)
	}
}

func DeserializeJSON(reader io.Reader) (*ranking.DomainRankings, error) {
	jsonSerDe := serde.NewJSONSerDe()
	domainRankings, err := jsonSerDe.DeserializeFromReader(reader)
	if err != nil {
		return nil, icli.ErrIOFailure(err.Error())
	}
	return domainRankings, nil
}

func DeserializeCSV(reader io.Reader) (*ranking.DomainRankings, error) {
	rankings, err := csv.ReadRankingsCSV(reader)
	if err != nil {
		return nil, icli.ErrIOFailure(err.Error())
	}

	dr := ranking.NewDomainRankings("", "", NowUTC(), ranking.MixLevelDomains, rankings)

	return dr, nil
}

func NowUTC() time.Time {
	return time.Now().UTC()
}

func PrintDomainRankings(ranking *ranking.DomainRankings) {
	log.Printf("Provider: %s", ranking.Provider)
	log.Printf("Reference: %s", ranking.Reference)
	log.Printf("Ranking Type: %s", ranking.RankingType)
	log.Printf("Published At: %s", ranking.PublishedAt)
	log.Printf("Created At: %s", ranking.CreatedAt)
}
