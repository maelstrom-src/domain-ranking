package main

import (
	"io"
	"os"

	"github.com/urfave/cli/v2"
	icli "gitlab.com/maelstrom-src/domain-ranking/internal/cli"
	"gitlab.com/maelstrom-src/domain-ranking/internal/providers"
)

const (
	getCommandName      = "get"
	getCommandAlias     = "g"
	getCommandUsage     = "Get domain ranking list from provider"
	getCommandArgsUsage = "[provider]"
)

var GetCommand = &cli.Command{
	Name:      getCommandName,
	Aliases:   []string{getCommandAlias},
	Usage:     getCommandUsage,
	ArgsUsage: getCommandArgsUsage,
	Flags: []cli.Flag{
		outputFlag,
		outputFormatFlag,
	},
	Action: getAction,
}

func getAction(c *cli.Context) error {
	args := c.Args()
	if !args.Present() {
		return icli.ErrMissingArgs("No provider selected")
	}

	if args.Len() > 1 {
		return icli.ErrInvalidArgs("Only one provider can be selected")
	}

	outputFormat := c.String(outputFormatFlagName)
	if !IsIOFormatValid(outputFormat) {
		return icli.ErrInvalidOutputFormat(outputFormat)
	}

	var writer io.WriteCloser

	output := c.String(outputFlagName)
	if IsStdIO(output) {
		writer = os.Stdout
	} else {
		var err error
		writer, err = CreateFile(output)
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	selectedProvider := args.Get(0)
	provider, err := providers.Get(selectedProvider)
	if err != nil {
		return icli.ErrInvalidArgs(err.Error())
	}

	domainRankings, err := provider.GetLatest()
	if err != nil {
		return icli.ErrProviderFailure(err.Error())
	}

	PrintDomainRankings(domainRankings)

	return Serialize(outputFormat, writer, domainRankings)
}
