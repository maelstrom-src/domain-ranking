module gitlab.com/maelstrom-src/domain-ranking

go 1.16

require (
	github.com/urfave/cli/v2 v2.3.0
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.26.0
)
