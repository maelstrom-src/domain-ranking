GO ?= go
GOFMT ?= gofmt
GOVET ?= $(GO) vet
GOENV ?= $(GO) env
GOVERSION ?= $(GO) version
GOBUILD ?= $(GO) build

GIT ?= git
DATE ?= date
SHELL := bash

GOOPTS ?=

GOOS ?= $(shell $(GOENV) GOOS)
GOARCH ?= $(shell $(GOENV) GOARCH)

ROOT_PACKAGE := gitlab.com/maelstrom-src/domain-ranking
VERSION_PACKAGE := $(ROOT_PACKAGE)/internal/version

GO_VERSION ?= $(shell $(GOVERSION))
GO_VERSION_NUMBER ?= $(word 3, $(GO_VERSION))

GIT_COMMIT := $(shell $(GIT) rev-parse HEAD)
GIT_TAG := $(shell $(GIT) describe --abbrev=0 --tags)
BUILD_DATE := $(shell $(DATE) '+%Y%m%d')

LDFLAG_GIT_COMMIT := "$(VERSION_PACKAGE).gitCommit"
LDFLAG_GIT_TAG := "$(VERSION_PACKAGE).gitTag"
LDFLAG_BUILD_DATE := "$(VERSION_PACKAGE).buildDate"
LDFLAG_VERSION := "$(VERSION_PACKAGE).version"

LDFLAGS = -w -s \
	-X $(LDFLAG_GIT_COMMIT)=$(GIT_COMMIT) \
	-X $(LDFLAG_GIT_TAG)=$(GIT_TAG) \
	-X $(LDFLAG_BUILD_DATE)=$(BUILD_DATE) \
	-X $(LDFLAG_VERSION)=$(GIT_TAG)

GOFILES ?= $(shell git ls-files '*.go')

.PHONY: fmt
fmt:
	$(GOFMT) -s -w $(GOFILES)

.PHONY: build-server
build-server:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOBUILD) -ldflags="$(LDFLAGS)" \
		-o bin/dr-server ./cmd/server/

.PHONY: build-client
build-client:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOBUILD) -ldflags="$(LDFLAGS)" \
		-o bin/dr-client ./cmd/client/

.PHONY: build-lists
build-lists:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOBUILD) -ldflags="$(LDFLAGS)" \
		-o bin/dr-lists ./cmd/list/

.PHONY: proto-go
proto-go:
	$(SHELL) internal/proto/generate.sh


.PHONY: setup-python
setup-python:
	python3 -m venv python/venv
	python/venv/bin/pip install --upgrade pip
	python/venv/bin/pip install wheel
	python/venv/bin/pip install grpcio
	python/venv/bin/pip install grpcio-tools

.PHONY: proto-python
proto-python:
	$(SHELL) python/generate.sh
