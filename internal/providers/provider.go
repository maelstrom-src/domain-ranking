package providers

import (
	"time"

	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

type Provider interface {
	Name() string
	GetLatest() (*ranking.DomainRankings, error)
	GetByTime(time.Time) (*ranking.DomainRankings, error)
}
