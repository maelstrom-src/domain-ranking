package tranco_list

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"time"

	"gitlab.com/maelstrom-src/domain-ranking/internal/archive"
	"gitlab.com/maelstrom-src/domain-ranking/internal/csv"
	ihttp "gitlab.com/maelstrom-src/domain-ranking/internal/http"
	"gitlab.com/maelstrom-src/domain-ranking/internal/providers"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

const (
	listTrancoList string = "tranco_list"

	filenameCSVTop1M string = "top-1m.csv"

	trancoListBaseUrl       string = "https://tranco-list.eu"
	trancoListReferencePath string = "/list/%s"
	trancoListPath          string = "/list/%s/1000000"
	trancoLatestListId      string = "/top-1m-id"
	trancoDailyListIdPath   string = "/daily_list_id"
	trancoDownloadDailyPath string = "/download_daily/%s"
	trancoDownloadPath      string = "/download/%s/1000000"
)

var (
	errZIPNotAvailable = errors.New("tranco_list: ZIP not available")
)

type TrancoList struct {
	httpClient ihttp.HTTPClient
}

func NewTrancoList() *TrancoList {
	baseUrl, err := url.Parse(trancoListBaseUrl)
	if err != nil {
		panic(err)
	}

	return &TrancoList{httpClient: ihttp.NewHTTPClient(baseUrl)}
}

func (t *TrancoList) Name() string {
	return listTrancoList
}

func (t *TrancoList) fetchRoute(path string, parameters map[string]string) (string, error) {
	return t.httpClient.GetString(path, parameters)
}

func (t *TrancoList) getLatestListId() (string, error) {
	content, err := t.fetchRoute(trancoLatestListId, nil)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

func (t *TrancoList) getListIdByTime(time time.Time) (string, error) {
	params := map[string]string{"date": t.formatTimeAsDate(time)}

	content, err := t.fetchRoute(trancoDailyListIdPath, params)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

func (t *TrancoList) formatTimeAsDate(time time.Time) string {
	return time.Format("2006-01-02")
}

func (t *TrancoList) downloadDaily(listId string) (*ranking.DomainRankings, error) {
	route := fmt.Sprintf(trancoDownloadDailyPath, listId)

	resp, err := t.httpClient.Get(route, nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusForbidden {
		return nil, errZIPNotAvailable
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errorf("failed to get daily list '%s': %s", listId, resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errorf("Failed to download daily list '%s': %s", listId, err)
	}

	contentBytes, err := archive.UnZIP(data, filenameCSVTop1M)
	if err != nil {
		return nil, err
	}

	lastModifiedTime, err := ihttp.GetLastModified(resp)
	if err != nil {
		return nil, errorf("failed to get Last-Modified for '%s'", listId)
	}

	domainRankings, err := t.createDomainRankings(listId, lastModifiedTime, string(contentBytes))
	if err != nil {
		return nil, err
	}

	return domainRankings, nil
}

func (t *TrancoList) download(listId string) (*ranking.DomainRankings, error) {
	route := fmt.Sprintf(trancoDownloadPath, listId)

	resp, err := t.httpClient.Get(route, nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errorf("failed to get list '%s': %s", listId, resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errorf("failed to download list '%s': %s", listId, err)
	}

	// TODO: Not present!!!
	lastModifiedTime, err := ihttp.GetLastModified(resp)
	if err != nil {
		return nil, errorf("failed to get Last-Modified for '%s'", listId)
	}

	domainRankings, err := t.createDomainRankings(listId, lastModifiedTime, string(data))
	if err != nil {
		return nil, err
	}

	return domainRankings, nil
}

func (t *TrancoList) createDomainRankings(
	listId string,
	publishedAt time.Time,
	rankingsCSV string,
) (*ranking.DomainRankings, error) {
	rankings, err := csv.ParseRankingsCSV(rankingsCSV)
	if err != nil {
		return nil, err
	}

	dr := ranking.NewDomainRankings(
		listTrancoList,
		t.createReference(listId),
		publishedAt,
		ranking.PayLevelDomains,
		rankings,
	)

	return dr, nil
}

func (t *TrancoList) createReference(listId string) string {
	return path.Join(trancoListBaseUrl, fmt.Sprintf(trancoListReferencePath, listId))
}

func (t *TrancoList) GetLatest() (*ranking.DomainRankings, error) {
	listId, err := t.getLatestListId()
	if err != nil {
		return nil, err
	}

	return t.getByListId(listId)
}

func (t *TrancoList) GetByTime(time time.Time) (*ranking.DomainRankings, error) {
	listId, err := t.getListIdByTime(time)
	if err != nil {
		return nil, err
	}

	return t.getByListId(listId)
}

func (t *TrancoList) getByListId(listId string) (*ranking.DomainRankings, error) {
	rankingList, err := t.downloadDaily(listId)
	if err != nil && err == errZIPNotAvailable {
		rankingList, err = t.download(listId)
	}
	if err != nil {
		return nil, err
	}

	return rankingList, nil
}

func errorf(format string, a ...interface{}) error {
	return fmt.Errorf("tranco_list: "+format, a...)
}

func init() {
	providers.Add(listTrancoList, func() providers.Provider {
		return NewTrancoList()
	})
}
