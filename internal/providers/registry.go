package providers

import (
	"fmt"
	"sort"
)

type Creator func() Provider

var providers = map[string]Creator{}

func Available() []string {
	names := make([]string, 0, len(providers))
	for name := range providers {
		names = append(names, name)
	}

	// Sort avaiable providers.
	sort.Strings(names)

	return names
}

func Get(name string) (Provider, error) {
	creator, ok := providers[name]
	if !ok {
		return nil, fmt.Errorf("Unknown provider: %s", name)
	}
	return creator(), nil
}

func Add(name string, creator Creator) {
	providers[name] = creator
}
