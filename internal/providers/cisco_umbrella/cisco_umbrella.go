package cisco_umbrella

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/maelstrom-src/domain-ranking/internal/archive"
	"gitlab.com/maelstrom-src/domain-ranking/internal/csv"
	ihttp "gitlab.com/maelstrom-src/domain-ranking/internal/http"
	"gitlab.com/maelstrom-src/domain-ranking/internal/providers"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

const (
	listTop1Million    string = "cisco_umbrella_top_1m"
	listTop1MillionTLD string = "cisco_umbrella_top_1m_tld"

	ciscoUmbrellaBaseURL string = "http://s3-us-west-1.amazonaws.com"

	filenameCSVTop1M    string = "top-1m.csv"
	filenameCSVTop1MTLD string = "top-1m-TLD.csv"

	// filenameCSVTop1M    string = "top-1m-<YYYY-MM-DD>.csv"
	// filenameCSVTop1MTLD string = "top-1m-TLD-<YYYY-MM-DD>.csv"
	historicalFilenameCSVTop1M    string = "top-1m-%s.csv"
	historicalFilenameCSVTop1MTLD string = "top-1m-TLD-%s.csv"

	listZIPPath string = "/umbrella-static/%s.zip"
)

type CiscoUmbrella struct {
	list                      string
	baseURL                   string
	filename                  string
	historicalFilenamePattern string
	rankingType               ranking.DomainRankingType

	httpClient ihttp.HTTPClient
}

func NewCiscoUmbrella(
	name, baseURL, filename, historicalFilenamePattern string,
	rankingType ranking.DomainRankingType,
) *CiscoUmbrella {
	baseUrl, err := url.Parse(baseURL)
	if err != nil {
		panic(err)
	}

	return &CiscoUmbrella{
		list:                      name,
		baseURL:                   baseURL,
		filename:                  filename,
		historicalFilenamePattern: historicalFilenamePattern,
		rankingType:               rankingType,
		httpClient:                ihttp.NewHTTPClient(baseUrl),
	}
}

func (c *CiscoUmbrella) Name() string {
	return c.list
}

func (c *CiscoUmbrella) GetLatest() (*ranking.DomainRankings, error) {
	path := fmt.Sprintf(listZIPPath, c.filename)
	return c.getByPath(path)
}

func (c *CiscoUmbrella) getByPath(path string) (*ranking.DomainRankings, error) {
	resp, err := c.httpClient.Get(path, nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errorf("failed to get '%s': %s", c.list, resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errorf("failed to download '%s': %s", c.list, err)
	}

	contentBytes, err := archive.UnZIP(data, c.filename)
	if err != nil {
		return nil, err
	}

	lastModifiedTime, err := ihttp.GetLastModified(resp)
	if err != nil {
		return nil, errorf("failed to get Last-Modified for '%s'", c.list)
	}

	domainRankings, err := c.createDomainRankings(c.list, path, lastModifiedTime, string(contentBytes))
	if err != nil {
		return nil, err
	}

	return domainRankings, nil
}

func (c *CiscoUmbrella) createDomainRankings(
	list, path string,
	publishedAt time.Time,
	rankingsCSV string,
) (*ranking.DomainRankings, error) {
	rankings, err := csv.ParseRankingsCSV(rankingsCSV)
	if err != nil {
		return nil, err
	}

	dr := ranking.NewDomainRankings(
		list,
		c.createReference(path),
		publishedAt,
		c.rankingType,
		rankings,
	)

	return dr, nil
}

func (c *CiscoUmbrella) createReference(listPath string) string {
	return fmt.Sprintf("%s%s", c.baseURL, listPath)
}

func (c *CiscoUmbrella) GetByTime(time time.Time) (*ranking.DomainRankings, error) {
	timestamp := fmt.Sprintf("%d-%02d-%02d", time.Year(), time.Month(), time.Day())
	filename := fmt.Sprintf(c.historicalFilenamePattern, timestamp)

	path := fmt.Sprintf(listZIPPath, filename)
	return c.getByPath(path)
}

func errorf(format string, a ...interface{}) error {
	return fmt.Errorf("cisco_umbrella: "+format, a...)
}

func init() {
	providers.Add(listTop1Million, func() providers.Provider {
		return NewCiscoUmbrella(
			listTop1Million,
			ciscoUmbrellaBaseURL,
			filenameCSVTop1M,
			historicalFilenameCSVTop1M,
			ranking.MixLevelDomains,
		)
	})
	providers.Add(listTop1MillionTLD, func() providers.Provider {
		return NewCiscoUmbrella(
			listTop1MillionTLD,
			ciscoUmbrellaBaseURL,
			filenameCSVTop1MTLD,
			historicalFilenameCSVTop1MTLD,
			ranking.TopLevelDomains,
		)
	})
}
