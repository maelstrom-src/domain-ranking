package version

import "fmt"

var (
	gitCommit = "unspecified"
	gitTag    = "unspecified"
	buildDate = "unspecified"
	version   = "unspecified"
)

type Info struct {
	GitCommit string
	GitTag    string
	BuildDate string
	Version   string
}

func Get() Info {
	return Info{
		GitCommit: gitCommit,
		GitTag:    gitTag,
		BuildDate: buildDate,
		Version:   version,
	}
}

func (i Info) String() string {
	return fmt.Sprintf(
		`{"Version": "%s", "GitCommit": "%s", "GitTag": "%s", "BuildDate": "%s"}`,
		i.Version,
		i.GitCommit,
		i.GitTag,
		i.BuildDate,
	)
}
