package http

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"
)

const (
	headerLastModified string = "Last-Modified"
)

var (
	errNoLastModifiedHeader = errors.New("client: response missing Last-Modified header")
)

type HTTPClient interface {
	Get(route string, params map[string]string) (*http.Response, error)
	GetString(route string, params map[string]string) (string, error)
}

type httpClient struct {
	baseURL *url.URL

	client *http.Client
}

func NewHTTPClient(baseURL *url.URL) HTTPClient {
	url := *baseURL
	return &httpClient{baseURL: &url, client: &http.Client{}}
}

func (c *httpClient) buildURL(route string, parameters map[string]string) (*url.URL, error) {
	if strings.HasPrefix(route, "/") {
		url := buildURL(c.baseURL, route, parameters)
		return url, nil
	} else {
		// TODO: Support for absolute URLs?
		return nil, fmt.Errorf("Not a relative URL: %s", route)
	}
}

func buildURL(baseURL *url.URL, route string, parameters map[string]string) *url.URL {
	url := *baseURL
	url.Path = path.Join(url.Path, route)

	if parameters != nil {
		query := url.Query()
		for key, value := range parameters {
			query.Set(key, value)
		}
		url.RawQuery = query.Encode()
	}

	return &url
}

func (c *httpClient) Get(route string, parameters map[string]string) (resp *http.Response, err error) {
	url, err := c.buildURL(route, parameters)
	if err != nil {
		return nil, err
	}
	return c.client.Get(url.String())
}

func (c *httpClient) GetString(route string, parameters map[string]string) (string, error) {
	resp, err := c.Get(route, parameters)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", errorf("failed to get '%s': %s", route, resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errorf("failed to read '%s': %s", route, err)
	}

	return string(data), nil
}

func errorf(format string, a ...interface{}) error {
	return fmt.Errorf("client: "+format, a...)
}

func GetLastModified(resp *http.Response) (time.Time, error) {
	lastModified := resp.Header.Get(headerLastModified)
	if lastModified == "" {
		return time.Now().UTC(), errNoLastModifiedHeader
	}

	return http.ParseTime(lastModified)

}
