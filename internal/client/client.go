package client

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	pb "gitlab.com/maelstrom-src/domain-ranking/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

var (
	defaultTimeout time.Duration = 10 * time.Second
)

var (
	errClientMethodNotImplemented = errors.New("client: method not implemented")
	errClientNotConnected         = errors.New("client: not connected")
	errClientNoProviders          = errors.New("client: no providers")
	errClientMissingTime          = errors.New("client: missing time")

	ErrInternalError  = errors.New("internal error")
	ErrEmptyProvider  = errors.New("provider is an empty string")
	ErrEmptyDomain    = errors.New("domain is an empty string")
	ErrNoRankingFound = errors.New("no ranking found")
)

type Client interface {
	Connect() error
	Close() error
	GetProvider(provider string) (*Provider, error)
	GetProviders() ([]*Provider, error)
	GetProviderRankingByRank(provider string, rank uint32) (*Ranking, error)
	GetProviderRankingByDomain(provider string, domain string) (*Ranking, error)
	GetProviderRankings(provider string, offset, limit uint32) ([]*Ranking, error)
	GetRankingByRank(rank uint32) ([]*Ranking, error)
	GetRankingByDomain(domain string) ([]*Ranking, error)
}

type defaultClient struct {
	target string

	connection *grpc.ClientConn
	client     pb.DomainRankingClient
}

func NewClient(target string) Client {
	return &defaultClient{target: target}
}

func (c *defaultClient) Connect() error {
	var opts []grpc.DialOption
	// if *tls {
	// 	if *caFile == "" {
	// 		*caFile = data.Path("x509/ca_cert.pem")
	// 	}
	// 	creds, err := credentials.NewClientTLSFromFile(*caFile, *serverHostOverride)
	// 	if err != nil {
	// 		log.Fatalf("Failed to create TLS credentials %v", err)
	// 	}
	// 	opts = append(opts, grpc.WithTransportCredentials(creds))
	// } else {
	// 	opts = append(opts, grpc.WithInsecure())
	// }
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	conn, err := grpc.Dial(c.target, opts...)
	if err != nil {
		return err
	}

	c.connection = conn
	c.client = pb.NewDomainRankingClient(c.connection)

	return nil
}

func (c *defaultClient) Close() error {
	if c.connection != nil {
		err := c.connection.Close()
		if err != nil {
			return err
		}
		c.client = nil
	}

	// TODO: return error if connection was not opened???
	return nil
}

func (c defaultClient) checkConnection() error {
	if c.connection == nil || c.client == nil {
		return errClientNotConnected
	}

	return nil
}

func (c defaultClient) GetProvider(provider string) (*Provider, error) {
	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Provider(ctx, &pb.ProviderRequest{Provider: provider})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return createProviderFromProviderResponse(response)
}

func createContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), defaultTimeout)
}

func createProviderFromProviderResponse(response *pb.ProviderResponse) (*Provider, error) {
	provider := response.GetProvider()
	if provider == nil {
		return nil, errClientNoProviders
	}

	return createProvider(provider)
}

func createProvider(p *pb.Provider) (*Provider, error) {
	pPublishedAt := p.GetPublishedAt()
	if pPublishedAt == nil {
		return nil, errClientMissingTime
	}

	pCreatedAt := p.GetCreatedAt()
	if pCreatedAt == nil {
		return nil, errClientMissingTime
	}

	provider := NewProvider(
		p.GetProvider(),
		p.GetReference(),
		pPublishedAt.AsTime(),
		pCreatedAt.AsTime(),
		p.GetRankingType(),
		p.GetRankingCount(),
	)

	return provider, nil
}

func (c defaultClient) GetProviders() ([]*Provider, error) {
	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Providers(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return createProvidersFromProvidersResponse(response)
}

func createProvidersFromProvidersResponse(response *pb.ProvidersResponse) ([]*Provider, error) {
	providers := response.GetProviders()
	if providers == nil {
		return nil, errClientNoProviders
	}

	var results []*Provider

	for _, provider := range providers {
		result, err := createProvider(provider)
		if err != nil {
			return nil, err
		}

		results = append(results, result)
	}

	return results, nil
}

func (c defaultClient) GetProviderRankingByRank(provider string, rank uint32) (*Ranking, error) {
	normalizedProvider := strings.TrimSpace(provider)
	if normalizedProvider == "" {
		return nil, ErrEmptyProvider
	}

	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Ranking(ctx, &pb.RankingRequest{Value: &pb.RankingRequest_Rank{Rank: rank}, Provider: normalizedProvider})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return handleSingleResultRankingResponse(response)
}

func handleSingleResultRankingResponse(response *pb.RankingResponse) (*Ranking, error) {
	rankings, err := createRankingsFromRankingResponse(response)
	if err != nil {
		return nil, err
	}

	rankingsCount := len(rankings)
	if rankingsCount == 0 {
		return nil, ErrNoRankingFound
	} else if rankingsCount > 1 {
		return nil, ErrInternalError
	}

	return rankings[0], nil
}

func createRankingsFromRankingResponse(response *pb.RankingResponse) ([]*Ranking, error) {
	var results []*Ranking

	rankings := response.GetRankings()
	if rankings == nil {
		return results, nil
	}

	for _, ranking := range rankings {
		result, err := createRanking(ranking)
		if err != nil {
			return nil, err
		}

		results = append(results, result)
	}

	return results, nil
}

func createRanking(r *pb.Ranking) (*Ranking, error) {
	pPublishedAt := r.GetPublishedAt()
	if pPublishedAt == nil {
		return nil, errClientMissingTime
	}

	pCreatedAt := r.GetCreatedAt()
	if pCreatedAt == nil {
		return nil, errClientMissingTime
	}

	ranking := NewRanking(
		r.GetRank(),
		r.GetDomain(),
		r.GetProvider(),
		pCreatedAt.AsTime(),
		pCreatedAt.AsTime(),
	)

	return ranking, nil
}

func (c defaultClient) GetProviderRankingByDomain(provider string, domain string) (*Ranking, error) {
	normalizedProvider := strings.TrimSpace(provider)
	if normalizedProvider == "" {
		return nil, ErrEmptyProvider
	}

	normalizedDomain := strings.TrimSpace(domain)
	if normalizedDomain == "" {
		return nil, ErrEmptyDomain
	}

	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Ranking(ctx, &pb.RankingRequest{Value: &pb.RankingRequest_Domain{Domain: normalizedDomain}, Provider: normalizedProvider})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return handleSingleResultRankingResponse(response)
}

func (c defaultClient) GetProviderRankings(provider string, offset, limit uint32) ([]*Ranking, error) {
	normalizedProvider := strings.TrimSpace(provider)
	if normalizedProvider == "" {
		return nil, ErrEmptyProvider
	}

	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	stream, err := c.client.ProviderRankings(ctx, &pb.ProviderRankingsRequest{Provider: normalizedProvider, Offset: offset, Limit: limit})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	var results []*Ranking

	for {
		response, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, handleGRPCError(err)
		}

		ranking := response.GetRanking()
		if ranking == nil {
			return nil, ErrInternalError
		}

		r, err := createRanking(ranking)
		if err != nil {
			return nil, err
		}

		results = append(results, r)
	}

	return results, nil
}

func (c defaultClient) GetRankingByRank(rank uint32) ([]*Ranking, error) {
	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Ranking(ctx, &pb.RankingRequest{Value: &pb.RankingRequest_Rank{Rank: rank}})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return createRankingsFromRankingResponse(response)
}

func (c defaultClient) GetRankingByDomain(domain string) ([]*Ranking, error) {
	normalizedDomain := strings.TrimSpace(domain)
	if normalizedDomain == "" {
		return nil, ErrEmptyDomain
	}

	if err := c.checkConnection(); err != nil {
		return nil, err
	}

	ctx, cancel := createContext()
	defer cancel()

	response, err := c.client.Ranking(ctx, &pb.RankingRequest{Value: &pb.RankingRequest_Domain{Domain: normalizedDomain}})
	if err != nil {
		return nil, handleGRPCError(err)
	}

	return createRankingsFromRankingResponse(response)
}

func handleGRPCError(err error) error {
	st, ok := status.FromError(err)
	if ok {
		code := st.Code()
		if codes.NotFound == code {
			return ErrNoRankingFound
		} else {
			return fmt.Errorf("Server error: %s (%s)", st.Message(), code.String())
		}
	} else {
		return err
	}
}
