package client

import (
	"time"
)

type Provider struct {
	Provider     string
	Referene     string
	PublishedAt  time.Time
	CreatedAt    time.Time
	RankingType  string
	RankingCount uint32
}

func NewProvider(
	provider string,
	reference string,
	publishedAt time.Time,
	createdAt time.Time,
	rankingType string,
	rankingCount uint32,
) *Provider {
	return &Provider{
		Provider:     provider,
		Referene:     reference,
		PublishedAt:  publishedAt,
		CreatedAt:    createdAt,
		RankingType:  rankingType,
		RankingCount: rankingCount,
	}
}

type Ranking struct {
	Rank        uint32
	Domain      string
	Provider    string
	PublishedAt time.Time
	CreatedAt   time.Time
}

func NewRanking(
	rank uint32,
	domain string,
	provider string,
	publishedAt time.Time,
	createdAt time.Time,
) *Ranking {
	return &Ranking{
		Rank:        rank,
		Domain:      domain,
		Provider:    provider,
		PublishedAt: publishedAt,
		CreatedAt:   createdAt,
	}
}
