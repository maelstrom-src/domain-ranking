package csv

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

func ReadRankingsCSV(reader io.Reader) (ranking.Rankings, error) {
	contentBytes, err := io.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	return ParseRankingsCSV(string(contentBytes))
}

func ReadRankingsCSVFile(filename string) (ranking.Rankings, error) {
	contentBytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return ParseRankingsCSV(string(contentBytes))
}

func ParseRankingsCSV(rankingsCSV string) (ranking.Rankings, error) {
	rankings := make(ranking.Rankings)

	csvReader := csv.NewReader(strings.NewReader(rankingsCSV))
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		rankInt, err := strconv.Atoi(record[0])
		if err != nil {
			return nil, err
		}

		rank := ranking.Rank(rankInt)
		domain := ranking.Domain(record[1])

		rankings.Set(rank, domain)
	}

	return rankings, nil
}

func WriteRankingsCSVFile(filename string, rankings ranking.Rankings) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	return WriteRankingsCSV(file, rankings)
}

func WriteRankingsCSV(writer io.Writer, rankings ranking.Rankings) error {
	csvWriter := csv.NewWriter(writer)

	ranks := rankings.Ranks()
	for _, rank := range ranks {
		domain, ok := rankings.Get(rank)
		if !ok {
			log.Printf("Rank: %d", rank)
			continue
		}

		row := []string{strconv.FormatUint(uint64(rank), 10), string(domain)}

		err := csvWriter.Write(row)
		if err != nil {
			return err
		}
	}

	csvWriter.Flush()

	return csvWriter.Error()
}
