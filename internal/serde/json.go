package serde

import (
	"encoding/json"
	"io"
	"os"

	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

type JSONSerDe struct {
}

func NewJSONSerDe() *JSONSerDe {
	return &JSONSerDe{}
}

func (j *JSONSerDe) Serialize(domainRankings *ranking.DomainRankings) ([]byte, error) {
	data, err := json.MarshalIndent(domainRankings, "", " ")
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (j *JSONSerDe) SerializeToWriter(writer io.Writer, domainRankings *ranking.DomainRankings) error {
	bytes, err := j.Serialize(domainRankings)
	if err != nil {
		return err
	}

	_, err = writer.Write(bytes)
	if err != nil {
		return err
	}

	return nil
}

func (j *JSONSerDe) SerializeToFile(filename string, domainRankings *ranking.DomainRankings) error {
	bytes, err := j.Serialize(domainRankings)
	if err != nil {
		return err
	}

	return os.WriteFile(filename, bytes, defaultFileMode)
}

func (j *JSONSerDe) Deserialize(data []byte) (*ranking.DomainRankings, error) {
	var domainRankings ranking.DomainRankings

	err := json.Unmarshal(data, &domainRankings)
	if err != nil {
		return nil, err
	}

	return &domainRankings, nil
}

func (j *JSONSerDe) DeserializeFromReader(reader io.Reader) (*ranking.DomainRankings, error) {
	bytes, err := io.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	return j.Deserialize(bytes)
}

func (j *JSONSerDe) DeserializeFromFile(filename string) (*ranking.DomainRankings, error) {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return j.Deserialize(bytes)
}
