package serde

import (
	"os"

	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
)

const defaultFileMode os.FileMode = 0644

type SerDe interface {
	Serialize(domainRankings *ranking.DomainRankings) ([]byte, error)
	SerializeToFile(filename string, domainRankings *ranking.DomainRankings) error
	Deserialize(data []byte) (*ranking.DomainRankings, error)
	DeserializeFromFile(filename string) (*ranking.DomainRankings, error)
}
