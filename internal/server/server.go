package server

import (
	"context"
	"fmt"
	"log"
	"math"
	"time"

	pb "gitlab.com/maelstrom-src/domain-ranking/internal/proto"
	"gitlab.com/maelstrom-src/domain-ranking/internal/ranking"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type domainRankingServer struct {
	pb.UnimplementedDomainRankingServer

	providers map[string]*ranking.DomainRankings
}

func (s *domainRankingServer) getDomainRankings(provider string) (*ranking.DomainRankings, error) {
	domainRankings, ok := s.providers[provider]
	if !ok {
		return nil, fmt.Errorf("Unknown provider: %s", provider)
	}
	return domainRankings, nil
}

func (s *domainRankingServer) Providers(ctx context.Context, request *emptypb.Empty) (*pb.ProvidersResponse, error) {
	log.Printf("Invoking Providers request: %v", request)

	var providers []*pb.Provider

	for _, provider := range s.providers {
		providers = append(providers, createProvider(provider))
	}

	return &pb.ProvidersResponse{Providers: providers}, nil
}

func (s *domainRankingServer) Provider(ctx context.Context, request *pb.ProviderRequest) (*pb.ProviderResponse, error) {
	log.Printf("Invoking Provider request: %v", request)

	provider := request.GetProvider()

	if provider == "" {
		return nil, statusInvalidArgument("Missing or empty provider")
	}

	domainRankings, err := s.getDomainRankings(provider)
	if err != nil {
		return nil, statusNotFound(err.Error())
	}

	return &pb.ProviderResponse{Provider: createProvider(domainRankings)}, nil
}

func (s *domainRankingServer) ProviderRankings(request *pb.ProviderRankingsRequest, stream pb.DomainRanking_ProviderRankingsServer) error {
	log.Printf("Invoking ProviderRankings request: %v", request)

	provider := request.GetProvider()

	if provider == "" {
		return statusInvalidArgument("Missing or empty provider")
	}

	domainRankings, err := s.getDomainRankings(provider)
	if err != nil {
		return statusNotFound(err.Error())
	}

	offset := request.GetOffset()

	start := offset + 1
	if start >= uint32(len(domainRankings.Rankings)) {
		return statusInvalidArgument(fmt.Sprintf("Invalid offset: %d", offset))
	}

	limit := request.GetLimit()
	rankingCount := uint32(len(domainRankings.Rankings))

	end := uint32(math.Min(float64(offset+limit), float64(rankingCount)))

	for i := start; i <= end; i++ {
		rank := ranking.Rank(i)

		domain, ok := domainRankings.Rankings.Get(rank)
		if !ok {
			continue
		}

		ranking := createRanking(
			domainRankings.Provider,
			domainRankings.PublishedAt,
			domainRankings.CreatedAt,
			rank,
			domain,
		)

		if err := stream.Send(&pb.ProviderRankingsResponse{Ranking: ranking}); err != nil {
			return err
		}
	}

	return nil
}

func (s *domainRankingServer) Ranking(ctx context.Context, request *pb.RankingRequest) (*pb.RankingResponse, error) {
	log.Printf("Invoking Ranking request: %v", request)

	rankRaw := request.GetRank()
	domainRaw := request.GetDomain()

	if rankRaw < 1 && domainRaw == "" {
		return nil, statusInvalidArgument("Missing rank or domain")
	}

	providerName := request.GetProvider()

	var err error
	var provider *ranking.DomainRankings

	if providerName != "" {
		provider, err = s.getDomainRankings(providerName)
		if err != nil {
			return nil, statusNotFound(err.Error())
		}
	}

	var rankings []*pb.Ranking

	if provider != nil {
		result, err := handleProvider(provider, rankRaw, domainRaw)
		if err != nil {
			return nil, err
		}
		rankings = append(rankings, result)
	} else {
		for _, provider := range s.providers {
			result, err := handleProvider(provider, rankRaw, domainRaw)
			if err != nil {
				continue
			}
			rankings = append(rankings, result)
		}
	}

	if len(rankings) == 0 {
		return nil, statusNotFound("Not rankings found")
	}

	return createRankingResponse(rankings), nil
}

func handleProvider(provider *ranking.DomainRankings, rank uint32, domain string) (*pb.Ranking, error) {
	if rank > 0 && domain != "" {
		// In theory this should never happen.
		return nil, statusInvalidArgument("Both rank and domain provided")
	}

	var err error
	var resultRank ranking.Rank = 0
	var resultDomain ranking.Domain = ""

	if rank > 0 {
		resultRank = ranking.Rank(rank)
		resultDomain, err = provider.Domain(resultRank)
	}

	if domain != "" {
		resultDomain = ranking.Domain(domain)
		resultRank, err = provider.Rank(resultDomain)
	}

	if err != nil {
		return nil, statusNotFound(err.Error())
	}

	result := createRanking(
		provider.Provider,
		provider.PublishedAt,
		provider.CreatedAt,
		resultRank,
		resultDomain,
	)

	return result, nil
}

func statusNotFound(msg string) error {
	return status.Error(codes.NotFound, msg)
}

func statusInvalidArgument(msg string) error {
	return status.Error(codes.InvalidArgument, msg)
}

func createRankingResponse(
	rankings []*pb.Ranking,
) *pb.RankingResponse {
	return &pb.RankingResponse{
		Rankings: rankings,
	}
}

func createRanking(
	provider string,
	publishedAt,
	createdAt time.Time,
	rank ranking.Rank,
	domain ranking.Domain,
) *pb.Ranking {
	return &pb.Ranking{
		Provider:    provider,
		PublishedAt: timestamppb.New(publishedAt),
		CreatedAt:   timestamppb.New(createdAt),
		Rank:        uint32(rank),
		Domain:      string(domain),
	}
}

func createProvider(provider *ranking.DomainRankings) *pb.Provider {
	return &pb.Provider{
		Provider:     provider.Provider,
		Reference:    provider.Reference,
		PublishedAt:  timestamppb.New(provider.PublishedAt),
		CreatedAt:    timestamppb.New(provider.CreatedAt),
		RankingType:  string(provider.RankingType),
		RankingCount: uint32(len(provider.Rankings)),
	}
}

func newServer(rankings []*ranking.DomainRankings) *domainRankingServer {
	providers := make(map[string]*ranking.DomainRankings, len(rankings))

	for _, r := range rankings {
		providers[r.Provider] = r
	}

	return &domainRankingServer{providers: providers}
}

func Register(server *grpc.Server, rankings []*ranking.DomainRankings) {
	pb.RegisterDomainRankingServer(server, newServer(rankings))
}
