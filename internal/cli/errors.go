package cli

import (
	"github.com/urfave/cli/v2"
)

const (
	exitCodeMissingArgs = iota + 10
	exitCodeInvalidArgs
	exitCodeInvalidIOFormat
	exitCodeIOFailure
	exitCodeProviderFailure
)

func ErrMissingArgs(message string) error {
	return exit(message, exitCodeMissingArgs)
}

func ErrInvalidArgs(message string) error {
	return exit(message, exitCodeInvalidArgs)
}

func ErrInvalidInputFormat(format string) error {
	return ErrInvalidIOFormat("Invalid input format: " + format)
}

func ErrInvalidOutputFormat(format string) error {
	return ErrInvalidIOFormat("Invalid output format: " + format)
}

func ErrInvalidIOFormat(message string) error {
	return exit(message, exitCodeInvalidIOFormat)
}

func ErrIOFailure(message string) error {
	return exit(message, exitCodeIOFailure)
}

func ErrProviderFailure(message string) error {
	return exit(message, exitCodeProviderFailure)
}

func exit(message string, existCode int) error {
	return cli.Exit(message, existCode)
}
