package ranking

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"time"
)

type Rank uint32
type Domain string

type DomainRankingType string

func (t DomainRankingType) String() string {
	return string(t)
}

type Ranks []Rank

func (r Ranks) Len() int           { return len(r) }
func (r Ranks) Less(i, j int) bool { return r[i] < r[j] }
func (r Ranks) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }
func (r Ranks) Sort()              { sort.Sort(r) }

type Rankings map[Rank]Domain

func (r Rankings) Ranks() Ranks {
	ranks := make(Ranks, 0, len(r))

	for rank := range r {
		ranks = append(ranks, rank)
	}

	ranks.Sort()

	return ranks
}

func (r Rankings) Get(rank Rank) (Domain, bool) {
	domain, ok := r[rank]
	return domain, ok
}

func (r Rankings) Set(rank Rank, domain Domain) {
	r[rank] = domain
}

const (
	MixLevelDomains DomainRankingType = "MIX"
	TopLevelDomains DomainRankingType = "TLD"
	PayLevelDomains DomainRankingType = "PLD"
)

type DomainRankings struct {
	Provider    string            `json:"provider"`
	Reference   string            `json:"reference"`
	PublishedAt time.Time         `json:"published_at"`
	CreatedAt   time.Time         `json:"created_at"`
	RankingType DomainRankingType `json:"ranking_type"`
	Rankings    Rankings          `json:"rankings"`

	reverseRankings map[Domain]Rank
}

func NewDomainRankings(
	provider string,
	reference string,
	publishedAt time.Time,
	rankingType DomainRankingType,
	rankings map[Rank]Domain,
) *DomainRankings {
	dr := &DomainRankings{
		Provider:        provider,
		Reference:       reference,
		PublishedAt:     publishedAt,
		CreatedAt:       time.Now().UTC(),
		RankingType:     rankingType,
		Rankings:        rankings,
		reverseRankings: make(map[Domain]Rank, len(rankings)),
	}

	dr.initReverseRankings()

	return dr
}

func (dr *DomainRankings) initReverseRankings() {
	for rank, domain := range dr.Rankings {
		dr.reverseRankings[domain] = rank
	}
}

func (dr *DomainRankings) Domain(rank Rank) (Domain, error) {
	domain, ok := dr.Rankings.Get(rank)
	if !ok {
		return "", fmt.Errorf("Rank not found: %d", rank)
	}

	return domain, nil
}

func (dr *DomainRankings) Rank(domain Domain) (Rank, error) {
	rank, ok := dr.reverseRankings[domain]
	if !ok {
		return 0, fmt.Errorf("Domain not ranked: %s", domain)
	}

	return rank, nil
}

func (dr *DomainRankings) UnmarshalJSON(data []byte) error {
	type TmpDomainRankings DomainRankings
	var tmp TmpDomainRankings

	err := json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}

	*dr = DomainRankings(tmp)

	dr.reverseRankings = make(map[Domain]Rank)
	dr.initReverseRankings()

	return nil
}

func (dr *DomainRankings) Filename(extension string) string {
	prefix := strings.ToLower(dr.Provider)

	publishedAt := dr.PublishedAt
	timestamp := fmt.Sprintf("%d%02d%02d", publishedAt.Year(), publishedAt.Month(), publishedAt.Day())

	return fmt.Sprintf("%s-%s.%s", prefix, timestamp, extension)
}
