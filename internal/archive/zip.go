package archive

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
)

var (
	errEmpty = errors.New("zip: empty ZIP")
)

func UnZIP(zippedData []byte, filename string) ([]byte, error) {
	zippedDataLength := int64(len(zippedData))
	bytesReader := bytes.NewReader(zippedData)

	zipReader, err := zip.NewReader(bytesReader, zippedDataLength)
	if err != nil {
		return nil, err
	}

	archivedFileCount := len(zipReader.File)
	if archivedFileCount == 0 {
		return nil, errEmpty
	}

	for _, file := range zipReader.File {
		if filename == file.Name {
			return readZipFile(file)
		}
	}

	return nil, fmt.Errorf("zip: file not found: %s", filename)
}

func readZipFile(zf *zip.File) ([]byte, error) {
	file, err := zf.Open()
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return ioutil.ReadAll(file)
}
