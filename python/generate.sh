#!/usr/bin/env bash

set -eu

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd "$DIR"

venv/bin/python -m grpc_tools.protoc \
       -I../internal/proto \
       --python_out=./src/domain_ranking/proto \
       --grpc_python_out=./src/domain_ranking/proto \
       ../internal/proto/domain_ranking.proto
