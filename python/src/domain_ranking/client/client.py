import grpc

import domain_ranking.proto.domain_ranking_pb2 as domain_ranking_pb2
import domain_ranking.proto.domain_ranking_pb2_grpc as domain_ranking_pb2_grpc

from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2

class Client:
    def __init__(self, address):
        self.address = address

    def test(self):
        with grpc.insecure_channel(self.address) as channel:
            stub = domain_ranking_pb2_grpc.DomainRankingStub(channel)

            try:
                response = stub.Ranking(domain_ranking_pb2.RankingRequest(domain='neti.ee'))
            except grpc.RpcError as rpc_error:
                status = rpc_error.code()
                if status is grpc.StatusCode.NOT_FOUND:
                    print(f"status: {rpc_error.code()}")
                    print(f"details: {rpc_error.details()}")
            else:
                # print(f"response: {response}")

                print(f"provider: {response.provider}")
                print(f"published_at: {response.published_at.ToDatetime()}")
                print(f"created_at: {response.created_at.ToDatetime()}")
                print(f"rank: {response.rank}")
                print(f"rank: {response.domain}")
